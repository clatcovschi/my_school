import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProjectBaseComponent } from './project-base/project-base.component';
import { PrincipalPageComponent } from './principal-page/principal-page.component';
import {RouterModule} from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';

@NgModule({
  declarations: [
    AppComponent,
    ProjectBaseComponent,
    PrincipalPageComponent,
    AboutUsComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      { path: '', component: ProjectBaseComponent },
      { path: 'Home', component: PrincipalPageComponent },
      { path: 'About', component: AboutUsComponent },
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
