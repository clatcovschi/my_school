import {Component, OnInit} from '@angular/core';
import * as Leaflet from 'leaflet';

@Component({
  selector: 'app-project-base',
  templateUrl: './project-base.component.html',
  styleUrls: ['./project-base.component.css']
})
export class ProjectBaseComponent implements OnInit {
  // Map
  show = true;

  title = 'leafletApps';
  map: Leaflet.Map;

  // Circles
  public innerHeight: any;
  public offsetHeight: any;
  public dots: any;
  bubbles = [];


// Random color
  // tslint:disable-next-line:typedef
  getRandomColor() {
      const color = Math.floor(0x1000 * Math.random()).toString(9);
      return '#' + ('0000' + color).slice(-4) + 'E0';
    }



  // tslint:disable-next-line:typedef
  ngOnInit() {





    // Map
    this.map = Leaflet.map('map').setView([47.9546242325, 27.5606537275], 13);
    Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
    ).addTo(this.map);

    Leaflet.marker([47.9546242325, 27.5606537275]).addTo(this.map).bindPopup('\n' +
      'Ne aflam aici').openPopup();
    // Numarul de buline
    this.innerHeight = window.innerHeight;
    this.offsetHeight = document.getElementById('class').offsetHeight;
    this.dots = Math.floor(((this.offsetHeight - document.getElementById('navbar').offsetHeight) / this.innerHeight) * 20);

// Create an object with styles
    let p = 0;
    let i = 0;
    while ( i < this.dots) {
      let a = Math.floor(i / 20);
      const X = Math.floor(1350 * Math.random());
      const Y = Math.floor( 900 * Math.random()) + 600 * a;
      const size = Math.floor(150 * Math.random());

      this.bubbles[i] = {
        id: i,
        color: this.getRandomColor(),
        size,
        translate: []
      };


      // verify if is without overlap
      let exist = true;
      if ( i >= 1){
      for (let j = 0; j < i; j++) {
        const d = Math.pow((Math.pow(X - this.bubbles[j].translate[0], 2)) +
          (Math.pow(Y - this.bubbles[j].translate[1], 2)), 0.5);
        const rr = this.bubbles[j].size + size;
        if (d < rr) {
          exist = false;
          p += 1;
          break;
        }
      }
      }
      // tslint:disable-next-line:triple-equals
      if ( exist == true){
        this.bubbles[i].translate[0] = X;
        this.bubbles[i].translate[1] = Y;
        i += 1;
      }

      a = 0;
    }
  }
}
